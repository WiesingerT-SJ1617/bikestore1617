﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;

namespace bikestore1617.Models
{
    public class CustomerModel
    {
        public ObjectId _id { get; set; }
        public String customernumber { get; set; }
        public String email { get; set; }
        public String firstname { get; set; }
        public String lastname { get; set; }
    }
}
