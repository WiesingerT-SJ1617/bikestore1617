﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;

namespace bikestore1617.Models
{
    public class BikeModel
    {
        public ObjectId _id { get; set; }
        public String color { get; set; }
        public String size { get; set; }
        public String description { get; set; }
        public double price { get; set; }
        public Boolean available { get; set; }
    }
}
