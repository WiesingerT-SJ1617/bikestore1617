﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using bikestore1617.Repo;
using bikestore1617.Models;

namespace bikestore1617.Controllers
{
    [Route("api/[controller]")]
    public class BikeController : Controller
    {
        protected RepoBike _repository;
        public BikeController()
        {
            _repository = new RepoBike();
        }

        [HttpGet]
        public List<BikeModel> Get(string jsonQuery = "")
        {
            if (jsonQuery == "") return _repository.SelectAll();
            return _repository.BikeFilter(jsonQuery);
        }

        public BikeModel Bike(BikeModel bikeModel, string id="")
        {
            if (id == "") return _repository.InsertBike(bikeModel);
            return _repository.UpdateBike(id, bikeModel);
        }
    }
}
