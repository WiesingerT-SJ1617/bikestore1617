﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;

namespace bikestore1617.Models
{
    public class ShopModel
    {
        public ObjectId _id { get; set; }
        public String name { get; set; }
        public String address { get; set; }
    }
}
