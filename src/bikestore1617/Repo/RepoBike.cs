﻿using bikestore1617.Models;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using MongoDB.Bson.Serialization;
using System.Linq;
using System.Threading.Tasks;

namespace bikestore1617.Repo
{
    public class RepoBike
    {
        protected static IMongoClient _client;
        protected static IMongoDatabase _database;
        protected IMongoCollection<BikeModel> _collection;

        public RepoBike()
        {
            _client = new MongoClient();
            _database = _client.GetDatabase("bike");
            _collection = _database.GetCollection<BikeModel>("bikestore");
        }

        public BikeModel Get(string id)
        {
            return this._collection.Find(new BsonDocument { { "_id", new ObjectId(id) } }).FirstAsync().Result;
        }

        public BikeModel InsertBike(BikeModel bike)
        {
            this._collection.InsertOneAsync(bike);
            return this.Get(bike._id.ToString());
        }

        public List<BikeModel> SelectAll()
        {
            var query = this._collection.Find(new BsonDocument()).ToListAsync();
            return query.Result;
        }
        
        public List<BikeModel> BikeFilter(string jsonQuery)
        {
            var queryFil = new QueryDocument(BsonSerializer.Deserialize<BsonDocument>(jsonQuery));
            return _collection.Find<BikeModel>(queryFil).ToList();
        }

        public BikeModel UpdateBike(string id, BikeModel bikemodel)
        {
            bikemodel._id = new ObjectId(id);

            var updatefilter = Builders<BikeModel>.Filter.Eq(s => s._id, bikemodel._id);
            this._collection.ReplaceOneAsync(updatefilter, bikemodel);
            return this.Get(id);
        }
    }
}
