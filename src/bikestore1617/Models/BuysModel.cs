﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;

namespace bikestore1617.Models
{
    public class BuysModel
    {
        public ObjectId _id { get; set; }
        public String dayofpurchase { get; set; }
    }
}
